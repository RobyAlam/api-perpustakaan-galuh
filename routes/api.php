<?php

use App\Http\Controllers\BukuController;
use App\Http\Controllers\KategoriController;
use App\Http\Controllers\PeminjamanController;
use App\Http\Controllers\PeminjamController;
use App\Http\Controllers\PengembalianController;
use App\Http\Controllers\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/login', [UserController::class, 'login']);

Route::prefix('user')->group(function () {
    Route::get('/detail/{id}', [UserController::class, 'detail']);
});

Route::prefix('kategori')->group(function () {
    Route::get('/', [KategoriController::class, 'index']);
});

Route::prefix('buku')->group(function () {
    Route::get('/', [BukuController::class, 'index']);
    Route::post('/insert', [BukuController::class, 'insert']);
});

Route::prefix('peminjam')->group(function () {
    Route::get('/', [PeminjamController::class, 'index']);
});

Route::prefix('peminjaman')->group(function () {
    Route::get('/', [PeminjamanController::class, 'index']);
    Route::post('/insert', [PeminjamanController::class, 'insert']);
    Route::get('/{id}', [PeminjamanController::class, 'detail']);
});

Route::prefix('pengembalian')->group(function () {
    Route::get('/', [PengembalianController::class, 'index']);
    Route::post('/insert', [PengembalianController::class, 'insert']);
});
