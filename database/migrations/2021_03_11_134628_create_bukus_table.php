<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBukusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('buku', function (Blueprint $table) {
            $table->id();
            $table->integer('id_kategori');
            $table->string('judul', 255);
            $table->string('penerbit', 100);
            $table->string('penulis', 100);
            $table->text('deskripsi');
            $table->integer('tahun_terbit');
            $table->integer('jumlah');
            $table->integer('jumlah_tersedia');
            $table->string('foto', 255)->nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('buku');
    }
}
