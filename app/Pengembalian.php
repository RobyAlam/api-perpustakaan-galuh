<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pengembalian extends Model
{
    protected $table = 'pengembalian';
    protected $fillable = [
        'id_peminjaman',
        'tgl_dikembalikan',
        'total_denda'
    ];
}
