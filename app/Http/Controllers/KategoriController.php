<?php

namespace App\Http\Controllers;

use App\Kategori;
use Illuminate\Http\Request;

class KategoriController extends Controller
{
    function index() {
        $kategori = Kategori::all();
        return response()->json([
            'data' => $kategori
        ]);
    }
}
