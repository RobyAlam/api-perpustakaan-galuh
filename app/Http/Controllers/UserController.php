<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    function login(Request $request)
    {
        $email = $request->email;
        $password = sha1($request->password);
        $user = User::where('email', $email)->where('password', $password)->first();
        if ($user) {
            return response()->json([
                'name'      => $user->name,
                'role'      => $user->role,
                'message'   => 'Berhasil Login',
                'status'    => 200
            ]);
        } else {
            return response()->json([
                'message'   => 'Username atau password salah!',
                'status'    => 500
            ]);
        }
    }

    function detail($id)
    {
        $user = User::find($id);
        return response()->json([
            'user'  => $user
        ]);
    }
}
