<?php

namespace App\Http\Controllers;

use App\DetailPeminjaman;
use App\DetailPengembalian;
use App\Pengembalian;
use Illuminate\Http\Request;

class PengembalianController extends Controller
{
    function index(Request $request)
    {
        $pengembalian = Pengembalian::select('pengembalian.*', 'peminjaman.id')->leftjoin('peminjaman', 'peminjaman.id', '=', 'pengembalian.id');

        if(!empty($request->perPage)) {
            $pengembalian = $pengembalian->orderBy('pengembalian.id', 'desc')->paginate($request->perPage);
        } else {
            $pengembalian = $pengembalian->orderBy('pengembalian.id', 'desc')->paginate(10);
        }


        return response()->json([
            'list' => $pengembalian
        ]);
    }

    function insert(Request $request)
    {
        $peminjaman = $request->list;
        if (count($peminjaman)>0) {
            foreach ($peminjaman as $pnjm) {
                $pengembalian = Pengembalian::create([
                    'id_peminjaman'   => $pnjm['id'],
                    'tgl_dikembalikan'  => now(),
                    'total_denda'       => 0
                ]);

                $detail_peminjaman = DetailPeminjaman::where('id_peminjaman', $pnjm['id'])->get();
                foreach ($detail_peminjaman as $dtl) {
                    $detail = DetailPengembalian::create([
                        'id_pengembalian' => $pengembalian->id,
                        'id_buku'   => $dtl['id_buku'],
                        'denda' => 0
                    ]);
                }
            }

            return response()->json([
                'message' => 'Data pengembalian berhasil di buat',
            ]);
        } else {
            return response()->json([
                'message' => 'Data pengembalian gagal di buat',
            ]);
        }
    }
}
