<?php

namespace App\Http\Controllers;

use App\Buku;
use App\Kategori;
use Illuminate\Http\Request;

class BukuController extends Controller
{
    function index(Request $request) {
        $buku = Buku::select('buku.*', 'kategori.nama as kategori')
                    ->leftjoin('kategori', 'buku.id_kategori', '=', 'kategori.id');
        $kategori = Kategori::all();

        if (!empty($request->searchKey)) {
            $buku = $buku->where('buku.judul', 'like', '%'.$request->searchKey.'%');
        }

        if (!empty($request->kategori)) {
            $buku = $buku->where('kategori.id', $request->kategori);
        }

        $buku = $buku->orderBy('buku.id', 'desc')->paginate(10);
        return response()->json([
            'buku'      => $buku,
            'kategori'  => $kategori
        ]);
    }

    function insert(Request $request)
    {
        $this->validate($request, [
            'judul'     => 'required',
            'kategori'  => 'required',
            'penulis'   => 'required',
            'penerbit'  => 'required',
            'tahun'     => 'required',
            'jumlah'    => 'required',
            'deskripsi' => 'required',
            'foto'      => 'required'
        ]);
        $file = $request->file('foto');

        $nama_file = time()."_".$file->getClientOriginalName();
        $buku = Buku::create([
            'judul'         => $request->judul,
            'id_kategori'   => $request->kategori,
            'penulis'       => $request->penulis,
            'penerbit'      => $request->penerbit,
            'tahun_terbit'  => $request->tahun,
            'jumlah'        => $request->jumlah,
            'jumlah_tersedia' => $request->jumlah,
            'deskripsi'     => $request->deskripsi,
            'foto'          => $nama_file
        ]);

        if ($buku) {
            $folder_name = 'assets/buku';
            $file->move($folder_name, $nama_file);

            return response()->json([
                'data'  => $buku,
                'message'   => 'Data buku berhasil disimpan'
            ]);
        } else {
            return response()->json([
                'message'   => 'Terjadi kesalahan! Data buku gagal disimpan'
            ]);
        }

    }
}
