<?php

namespace App\Http\Controllers;

use App\Peminjam;
use Illuminate\Http\Request;

class PeminjamController extends Controller
{
    function index(Request $request)
    {
        $peminjam = Peminjam::select('*');

        if (!empty($request->search)) {
            $peminjam = $peminjam->where('nama', 'like', '%'.$request->search.'%');
        }

        if (!empty($request->perPage)) {
            $peminjam = $peminjam->orderBy('id', 'desc')->paginate($request->perPage);
        } else {
            $peminjam = $peminjam->orderBy('id', 'desc')->paginate(10);
        }

        return response()->json([
            'data' => $peminjam,
        ]);
    }

}
