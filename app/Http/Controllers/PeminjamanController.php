<?php

namespace App\Http\Controllers;

use App\Buku;
use App\DetailPeminjaman;
use App\Peminjam;
use App\Peminjaman;
use Illuminate\Http\Request;

class PeminjamanController extends Controller
{

    function index(Request $request)
    {
        $peminjaman = Peminjaman::select('peminjaman.*', 'peminjam.nama', 'peminjam.no_telp')->leftjoin('peminjam', 'peminjam.id', '=', 'peminjaman.id_peminjam');

        if(!empty($request->search)) {
            $peminjaman = $peminjaman->where('peminjam.nama', 'like', '%'.$request->search.'%')->orWhere('peminjam.no_telp', 'like', '%'.$request->search.'%');
        }

        if(!empty($request->pinjamDari) && !empty($request->pinjamKe)) {
            $peminjaman = $peminjaman->whereBetween('peminjaman.created_at', [$request->pinjamDari, $request->pinjamKe]);
        }

        if (!empty($request->kembaliDari) && !empty($request->kembaliKe)) {
            $peminjaman = $peminjaman->whereBetween('peminjaman.tgl_kembali', [$request->kembaliDari, $request->kembaliKe]);
        }

        if(!empty($request->perPage)) {
            $peminjaman = $peminjaman->orderBy('peminjaman.id', 'desc')->paginate($request->perPage);
        } else {
            $peminjaman = $peminjaman->orderBy('peminjaman.id', 'desc')->paginate(10);
        }

        return response()->json([
            'list' => $peminjaman
        ]);
    }

    function insert(Request $request)
    {
        $this->validate($request, [
            'nama'     => 'required',
            'email'  => 'required',
            'no_telp'   => 'required',
            'alamat'  => 'required',
            'tgl_kembali'   => 'required'
        ]);
        $buku = $request->buku;
        if (count($request->buku) > 0) {
            $peminjam = Peminjam::where('email', $request->email)->first();
            if ($peminjam == null) {
                $peminjam = Peminjam::create([
                    'nama'      => $request->nama,
                    'email'     => $request->email,
                    'no_telp'   => $request->no_telp,
                    'alamat'    => $request->alamat
                ]);
            }

            $peminjaman = Peminjaman::create([
                'id_peminjam'   => $peminjam->id,
                'tgl_pinjam'    => now(),
                'tgl_kembali'   => $request->tgl_kembali,
            ]);

            foreach ($buku as $b) {
                $detail_peminjaman = DetailPeminjaman::create([
                    'id_peminjaman' => $peminjaman->id,
                    'id_buku'       => $b['id']
                ]);

                $book = Buku::find($b['id']);
                $book->jumlah_tersedia = $book->jumlah_tersedia - 1;
                $book->save();
            }

            return response()->json([
                'message' => 'Data peminjaman berhasil di buat',
            ]);
        } else {
            return response()->json([
                'message' => 'Data peminjaman gagal di buat',
            ]);
        }
    }

    function detail($id)
    {
        $peminjaman = Peminjaman::find($id);
        if ($peminjaman) {
            $detail = DetailPeminjaman::where('id_peminjaman', $peminjaman->id)
                            ->leftjoin('buku', 'buku.id', '=', 'detail_peminjaman.id_buku')
                            ->get();

            $peminjam = Peminjam::find($peminjaman->id_peminjam);
            return response()->json([
                'peminjaman'    => $peminjaman,
                'detail'        => $detail,
                'peminjam'      => $peminjam,
                'message'       => 'Data ditemukan',
                'status'        => 200
            ]);
        } else {
            return response()->json([
                'message'       => 'Data yang dimaksud tidak ada',
                'status'        => 500
            ]);
        }

    }
}
