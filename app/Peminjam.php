<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Peminjam extends Model
{
    protected $table = 'peminjam';
    protected $fillable = [
        'nama',
        'email',
        'no_telp',
        'alamat'
    ];
}
