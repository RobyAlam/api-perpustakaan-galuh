<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Buku extends Model
{
    protected $table = 'buku';
    protected $fillable = [
        'id_kategori',
        'judul',
        'penerbit',
        'penulis',
        'deskripsi',
        'tahun_terbit',
        'jumlah',
        'jumlah_tersedia',
        'foto'
    ];
}
